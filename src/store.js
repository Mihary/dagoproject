import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count:20,
    isProductSoldIn: true,
  },

  getters:{
    ProductSold: state =>{
      return state.isProductSoldIn
    }
  },
  mutations: {
    removeProduct(state,amount){
      state.count -= amount
    }
  },
  actions: {
    removeProduct(context,amount){
      if (context.state.count > amount){
        context.commit('removeProduct',amount)
      }
    }
  }
});
