
# dagospices
## Miharisoa ANDRIANANTENAINA et Diagne Bousso
## A project which consists of creating a smiple website with vue.js,


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
